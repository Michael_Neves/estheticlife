(function () {
    'use strict';

    const Connect   = require('connect');
    const Server    = require('serve-static');
    const Path      = require('path');

    const Port = 4001;

    Connect().use(Server(Path.join(__dirname))).listen(Port, function () {
        console.log(`O servidor esta rodando na porta: ${Port}`);
    });
})();